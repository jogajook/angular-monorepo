/*
 * Public API Surface of lib-ui-shared
 */

export * from './lib/lib-ui-shared.service';
export * from './lib/lib-ui-shared.component';
export * from './lib/lib-ui-shared.module';
