import { NgModule } from '@angular/core';
import { LibUiSharedComponent } from './lib-ui-shared.component';



@NgModule({
  declarations: [LibUiSharedComponent],
  imports: [
  ],
  exports: [LibUiSharedComponent]
})
export class LibUiSharedModule { }
