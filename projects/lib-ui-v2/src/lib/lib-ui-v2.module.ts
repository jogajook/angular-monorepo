import { NgModule } from '@angular/core';
import { LibUiV2Component } from './lib-ui-v2.component';



@NgModule({
  declarations: [LibUiV2Component],
  imports: [
  ],
  exports: [LibUiV2Component]
})
export class LibUiV2Module { }
