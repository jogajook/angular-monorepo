/*
 * Public API Surface of lib-ui-v2
 */

export * from './lib/lib-ui-v2.service';
export * from './lib/lib-ui-v2.component';
export * from './lib/lib-ui-v2.module';
