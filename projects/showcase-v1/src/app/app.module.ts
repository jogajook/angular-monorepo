import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LibUiSharedModule } from 'lib-ui-shared';
import { LibUiV1Module } from 'lib-ui-v1';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LibUiSharedModule,
    LibUiV1Module,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
