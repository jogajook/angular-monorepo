import { NgModule } from '@angular/core';
import { LibUiV1Component } from './lib-ui-v1.component';



@NgModule({
  declarations: [LibUiV1Component],
  imports: [
  ],
  exports: [LibUiV1Component]
})
export class LibUiV1Module { }
